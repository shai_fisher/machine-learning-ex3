import numpy as np
import matplotlib.pyplot as plt
import DataLoader
from NeuralNetwork import NeuralNetwork



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# plot the network performance
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def plotTrainCosts(iterations, batches, minibatch_costs,training_costs, validation_costs):
    """
    Plots costs graph for training iterations
    :param iterations: number of iterations
    :param batches: number of batches
    :param minibatch_costs: array of costs for each batch in each iteration
    :param training_costs: array of training costs
    :param validation_costs: array of costs of validation data
    """
    # Plot the minibatch, full training set, and validation costs
    minibatch_x_inds = np.linspace(0, iterations, num=iterations*batches)
    iteration_x_inds = np.linspace(1, iterations, num=iterations)

    # Plot the cost over the iterations
    plt.plot(minibatch_x_inds, minibatch_costs, 'k-', linewidth=0.5, label='cost minibatches')
    plt.plot(iteration_x_inds, training_costs, 'r-', linewidth=2, label='cost full training set')
    plt.plot(iteration_x_inds, validation_costs, 'b-', linewidth=3, label='cost validation set')
    # Add labels to the plot
    plt.xlabel('iteration')
    plt.ylabel('$\\xi$', fontsize=15)
    plt.title('Decrease of cost over backprop iteration')
    plt.legend()
    #x1,x2,y1,y2 = plt.axis()
    plt.axis((0,iterations,0,5))
    # plt.axis((0,nb_of_iterations,0,2.5))
    plt.grid()
    plt.show(block=False)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def plotConfusionTable(y_true, y_pred, columns):
    """
    Plots table of predicts for test data
    :param y_true: real clases
    :param y_pred: predicted classes
    :param columns: number of possible classes
    """
    mat = np.zeros([columns, columns], dtype=int)
    for (yt, yp) in zip(y_true, y_pred):
        mat[yt][yp] += 1
    #print mat
    plt.figure()
    plt.table(cellText=mat, rowLabels=range(columns), colLabels=range(columns), loc="center",
              colWidths = [0.05]*columns)
    plt.axis((0,columns*2,0,5))
    plt.show()

#********************** Main ***************************

MNIST_CONFIGURATION_SETS = [
    [[64, 20, 20, 10], 0.2],
    [[64, 64, 40, 20, 10], 0.4],
    [[64, 30, 10], 2],
]
CIFAR_CONFIGURATION_SETS = [
    [[3072, 20, 20, 10], 0.1],
    [[3072, 40, 10], 0.4],
    [[3072, 100, 50, 10], 2],
]
CYST_CONFIGURATION_SETS = [
    [[1521, 800, 200, 2], 0.3],
    [[1521, 40, 2], 0.5],
    [[1521, 1000, 400, 2], 0.1],
]
#-------------- print menu and get choice -----------------
action = -1
while (action != "0"):
    print "Select input data:"
    print "1 - MNIST"
    print "2 - CIFAR-10"
    print "3 - CYST"
    print "0 - Exit"
    action = raw_input ("Your choice: ")

    if action == "1":
        print "MNIST"
        print "Select configuration:"
        for i in range(len(MNIST_CONFIGURATION_SETS)):
            conf_set = MNIST_CONFIGURATION_SETS[i]
            print i, "- Layers:", conf_set[0], ", Learning Rate:", conf_set[1]
        conf_set_index = int(raw_input ("Your choice: "))
        conf_set = MNIST_CONFIGURATION_SETS[conf_set_index]
        # load images
        X_train, T_train, X_validation, T_validation, X_test, T_test = DataLoader.load_mnist_images()
        # define network
        network = NeuralNetwork(conf_set[0])
        # train
        iterations, batches, minibatch_costs, training_costs, validation_costs = \
            network.train(X_train, T_train, X_validation, T_validation, learning_rate=conf_set[1], max_iterations=300)
        plotTrainCosts(iterations, batches, minibatch_costs,training_costs, validation_costs)
        # test
        T_true, T_pred = network.test(X_test, T_test)
        plotConfusionTable(T_true, T_pred, 10)

    elif action == "2":
        print "CIFAR-10"
        # select configuration
        print "Select configuration:"
        for i in range(len(CIFAR_CONFIGURATION_SETS)):
            conf_set = CIFAR_CONFIGURATION_SETS[i]
            print i, "- Layers:", conf_set[0], ", Learning Rate:", conf_set[1]
        conf_set_index = int(raw_input ("Your choice: "))
        conf_set = CIFAR_CONFIGURATION_SETS[conf_set_index]
        print "Layers:", conf_set[0], ", Learning Rate:", conf_set[1]

        # load images
        X_train, T_train, X_validation, T_validation, X_test, T_test = DataLoader.load_cifar_images()
        # define network
        network = NeuralNetwork(conf_set[0])
        # train
        iterations, batches, minibatch_costs, training_costs, validation_costs = \
            network.train(X_train, T_train, X_validation, T_validation, learning_rate=conf_set[1], max_iterations=10)
        plotTrainCosts(iterations, batches, minibatch_costs,training_costs, validation_costs)
        # test
        T_true, T_pred = network.test(X_test, T_test)
        plotConfusionTable(T_true, T_pred, 10)

    elif action == "3":
        print "CYST"
        # select configuration
        print "Select configuration:"
        for i in range(len(CYST_CONFIGURATION_SETS)):
            conf_set = CYST_CONFIGURATION_SETS[i]
            print i, "- Layers:", conf_set[0], ", Learning Rate:", conf_set[1]
        conf_set_index = int(raw_input("Your choice: "))
        conf_set = CYST_CONFIGURATION_SETS[conf_set_index]
        print "Layers:", conf_set[0], ", Learning Rate:", conf_set[1]

        # load images
        X_train, T_train, X_validation, T_validation, X_test, T_test = DataLoader.load_cyst_images()
        # define network
        network = NeuralNetwork(conf_set[0])
        # train
        iterations, batches, minibatch_costs, training_costs, validation_costs = \
            network.train(X_train, T_train, X_validation, T_validation, learning_rate=conf_set[1], max_iterations=300)
        plotTrainCosts(iterations, batches, minibatch_costs, training_costs, validation_costs)
        # test
        T_true, T_pred = network.test(X_test, T_test)
        plotConfusionTable(T_true, T_pred, 2)

    elif action != "0":
        print "wrong choice"