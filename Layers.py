import itertools
import numpy as np

#********************** Math functions ***************************
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Define the non-linear functions used
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def logistic(z):
    return 1 / (1 + np.exp(-z))

def logistic_deriv(y):  # Derivative of logistic function
    return np.multiply(y, (1 - y))

def softmax(z):
    #print "softmax:", z[:1][:3]
    return np.exp(z) / np.sum(np.exp(z), axis=1, keepdims=True)

#********************** Layer abstract class ***************************

class Layer(object):
    """Base class for the different layers.
    Defines base methods and documentation of methods."""

    def get_params_iter(self):
        """Return an iterator over the parameters (if any).
        The iterator has the same order as get_params_grad.
        The elements returned by the iterator are editable in-place."""
        return []

    def get_params_grad(self, X, output_grad):
        """Return a list of gradients over the parameters.
        The list has the same order as the get_params_iter iterator.
        X is the input.
        output_grad is the gradient at the output of this layer.
        """
        return []

    def get_output(self, X):
        """Perform the forward step linear transformation.
        X is the input."""
        pass

    def get_input_grad(self, Y, output_grad=None, T=None):
        """Return the gradient at the inputs of this layer.
        Y is the pre-computed output of this layer (not needed in this case).
        output_grad is the gradient at the output of this layer
         (gradient at input of next layer).
        Output layer uses targets T to compute the gradient based on the
         output error instead of output_grad"""
        pass


#********************** Linear Layer class ***************************

class LinearLayer(Layer):

    def __init__(self, n_in, n_out):
        """
        initializes hidden layer parameters
        :param n_in: number of input variables
        :param n_out: number of output variables
        :return:
        """
        #print "layer init:", n_in, n_out
        self.W = np.random.randn(n_in, n_out) * 0.1     # weights
        #print "weights:", self.W[:5]
        self.b = np.zeros(n_out)

    def get_params_iter(self):
        """
        :return: Iterator over the parameters (W, b)
        """
        return itertools.chain(np.nditer(self.W, op_flags=['readwrite']),
                               np.nditer(self.b, op_flags=['readwrite']))

    def get_output(self, X):
        """
        Performs the forward step: inputs * weights ( + biases)
        :param X:
        :return:
        """
        return X.dot(self.W) + self.b

    def get_params_grad(self, X, output_grad):
        """

        :param X:
        :param output_grad:
        :return:
        """
        JW = X.T.dot(output_grad)
        Jb = np.sum(output_grad, axis=0)
        return [g for g in itertools.chain(np.nditer(JW), np.nditer(Jb))]

    def get_input_grad(self, Y, output_grad):
        return output_grad.dot(self.W.T)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#  LogisticLayer
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class LogisticLayer(Layer):
    """The logistic layer applies the logistic function to its"""

    def get_output(self, X):
        """Perform the forward step transformation."""
        return logistic(X)

    def get_input_grad(self, Y, output_grad):
        """Return the gradient at the inputs of this layer."""
        return np.multiply(logistic_deriv(Y), output_grad)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#  SoftmaxOutputLayer
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class SoftmaxOutputLayer(Layer):
    """The softmax output layer computes the classification propabilities at the output."""

    def get_output(self, X):
        """Perform the forward step transformation."""
        return softmax(X)

    def get_input_grad(self, Y, T):
        """Return the gradient at the inputs of this layer."""
        return (Y - T) / Y.shape[0]

    def get_cost(self, Y, T):
        """Return the cost at the output of this output layer."""
        return - np.multiply(T, np.log(Y)).sum() / Y.shape[0]
