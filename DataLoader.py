from PIL import Image
import numpy as np
import glob
from sklearn import datasets, svm, metrics, cross_validation

CYST_OK_FILE_PATTERN = '*OK*axial.png'
CYST_CYST_FILE_PATTERN = '*Cyst*axial.png'

#------------------------------------------------------

def load_mnist_images():
    """
    loads hand-writing digits data from package sklearn
    :return: digits images and labels as: [ (8x8 pixels matrix, label), ... ]
    """
    digits = datasets.load_digits()

    # init targets matrix target-size x 10 (possible digits), put '1' in digit column for each row
    num_images = digits.target.shape[0]
    T = np.zeros( (num_images, 10) )
    indexes = np.arange(num_images)     # [0, 1, 2, ..., num_images-1]
    T[indexes, digits.target] += 1

    # divide data and classes into train and test sets
    X_train, X_test, T_train, T_test = cross_validation.train_test_split(digits.data, T, test_size=0.4)

    # divide the test set into a validation set and final test set
    X_validation, X_test, T_validation, T_test = cross_validation.train_test_split(X_test, T_test, test_size=0.5)

    return X_train, T_train, X_validation, T_validation, X_test, T_test

#---------------------------------------------------
def loadImagesByList( file_pattern):
    # load images from files
    image_list = map(Image.open, glob.glob(file_pattern))
    imSizeAsVector = image_list[0].size[0] * image_list[0].size[1]
    images = np.zeros([len(image_list), imSizeAsVector])
    for idx, im in enumerate(image_list):
        images[idx,:] = np.array(im, np.uint8).reshape(imSizeAsVector, 1).T
    return images

#------------------------------------------------------
def get_splitted_data(all_images, images_classes):
    X_, X_test, y_, y_test = cross_validation.train_test_split(all_images, images_classes, test_size=0.20, random_state=42)
    X_train, X_val, y_train, y_val = cross_validation.train_test_split(X_, y_, test_size=0.20, random_state=42)
    #print "Total: ", len(all_images), "Train ", str(len(X_train)), ", Val: ", len(X_val), ", Test: ", len(X_test)

    # Normalize the data: substract the mean image
    mean_image = np.mean(X_train, axis=0)
    X_train -= mean_image
    X_val -= mean_image
    X_test -= mean_image

    return X_train, y_train, X_val, y_val, X_test, y_test
    #return X_train, y_train.T[0], X_val, y_val.T[0], X_test, y_test.T[0]
#------------------------------------------------------

def formatClasses(classes, width):
    classesArrays = []
    for classNumber in classes:
        classArray = np.zeros(width)
        classArray[classNumber] = 1
        classesArrays.append(classArray)
        #print classNumber, classArray
    return np.array(classesArrays)

#------------------------------------------------------
def load_cyst_images():
    ok_images = loadImagesByList("cyst\\" + CYST_OK_FILE_PATTERN)
    cyst_images = loadImagesByList("cyst\\" + CYST_CYST_FILE_PATTERN)
    images_classes = np.concatenate((np.zeros([ok_images.shape[0],1]),
                                np.ones([cyst_images.shape[0], 1]))).astype(int)
    all_images = np.concatenate((ok_images, cyst_images))

    images_classes = formatClasses(images_classes, 2)
    return get_splitted_data(all_images, images_classes)
#------------------------------------------------------
def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict
#------------------------------------------------------
def load_cifar_images():
    all_images = []
    all_classes = []
    for i in range(5):
        dict = unpickle('E:\\git\\MachineLearningEx3\\cifar-10\\data_batch_' + str(i+1))
        all_images.extend(dict.get('data'))
        all_classes.extend(dict.get('labels'))
    all_images = np.array(all_images).astype("float")
    all_classes = formatClasses(all_classes, 10)
    return get_splitted_data(all_images, all_classes)