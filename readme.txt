
NAME
    Layers

FILE
    e:\git\machinelearningex3\layers.py

CLASSES
    __builtin__.object
        Layer
            LinearLayer
            LogisticLayer
            SoftmaxOutputLayer
    
    class Layer(__builtin__.object)
     |  Base class for the different layers.
     |  Defines base methods and documentation of methods.
     |  
     |  Methods defined here:
     |  
     |  get_input_grad(self, Y, output_grad=None, T=None)
     |      Return the gradient at the inputs of this layer.
     |      Y is the pre-computed output of this layer (not needed in this case).
     |      output_grad is the gradient at the output of this layer
     |       (gradient at input of next layer).
     |      Output layer uses targets T to compute the gradient based on the
     |       output error instead of output_grad
     |  
     |  get_output(self, X)
     |      Perform the forward step linear transformation.
     |      X is the input.
     |  
     |  get_params_grad(self, X, output_grad)
     |      Return a list of gradients over the parameters.
     |      The list has the same order as the get_params_iter iterator.
     |      X is the input.
     |      output_grad is the gradient at the output of this layer.
     |  
     |  get_params_iter(self)
     |      Return an iterator over the parameters (if any).
     |      The iterator has the same order as get_params_grad.
     |      The elements returned by the iterator are editable in-place.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
    
    class LinearLayer(Layer)
     |  Method resolution order:
     |      LinearLayer
     |      Layer
     |      __builtin__.object
     |  
     |  Methods defined here:
     |  
     |  __init__(self, n_in, n_out)
     |      initializes hidden layer parameters
     |      :param n_in: number of input variables
     |      :param n_out: number of output variables
     |      :return:
     |  
     |  get_input_grad(self, Y, output_grad)
     |  
     |  get_output(self, X)
     |      Performs the forward step: inputs * weights ( + biases)
     |      :param X:
     |      :return:
     |  
     |  get_params_grad(self, X, output_grad)
     |      :param X:
     |      :param output_grad:
     |      :return:
     |  
     |  get_params_iter(self)
     |      :return: Iterator over the parameters (W, b)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from Layer:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
    
    class LogisticLayer(Layer)
     |  The logistic layer applies the logistic function to its
     |  
     |  Method resolution order:
     |      LogisticLayer
     |      Layer
     |      __builtin__.object
     |  
     |  Methods defined here:
     |  
     |  get_input_grad(self, Y, output_grad)
     |      Return the gradient at the inputs of this layer.
     |  
     |  get_output(self, X)
     |      Perform the forward step transformation.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Layer:
     |  
     |  get_params_grad(self, X, output_grad)
     |      Return a list of gradients over the parameters.
     |      The list has the same order as the get_params_iter iterator.
     |      X is the input.
     |      output_grad is the gradient at the output of this layer.
     |  
     |  get_params_iter(self)
     |      Return an iterator over the parameters (if any).
     |      The iterator has the same order as get_params_grad.
     |      The elements returned by the iterator are editable in-place.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from Layer:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
    
    class SoftmaxOutputLayer(Layer)
     |  The softmax output layer computes the classification propabilities at the output.
     |  
     |  Method resolution order:
     |      SoftmaxOutputLayer
     |      Layer
     |      __builtin__.object
     |  
     |  Methods defined here:
     |  
     |  get_cost(self, Y, T)
     |      Return the cost at the output of this output layer.
     |  
     |  get_input_grad(self, Y, T)
     |      Return the gradient at the inputs of this layer.
     |  
     |  get_output(self, X)
     |      Perform the forward step transformation.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Layer:
     |  
     |  get_params_grad(self, X, output_grad)
     |      Return a list of gradients over the parameters.
     |      The list has the same order as the get_params_iter iterator.
     |      X is the input.
     |      output_grad is the gradient at the output of this layer.
     |  
     |  get_params_iter(self)
     |      Return an iterator over the parameters (if any).
     |      The iterator has the same order as get_params_grad.
     |      The elements returned by the iterator are editable in-place.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from Layer:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)

FUNCTIONS
    logistic(z)
        #********************** Math functions ***************************
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        # Define the non-linear functions used
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    
    logistic_deriv(y)
    
    softmax(z)









NAME
    NeuralNetwork

FILE
    e:\git\machinelearningex3\neuralnetwork.py

CLASSES
    __builtin__.object
        NeuralNetwork
    
    class NeuralNetwork(__builtin__.object)
     |  Methods defined here:
     |  
     |  __init__(self, layers_input_sizes)
     |  
     |  backward_step(self, activations, targets)
     |      Perform the backpropagation step over all the layers and return the parameter gradients.
     |      Input:
     |          activations: A list of forward step activations where the activation at
     |              each index i+1 corresponds to the activation of layer i in layers.
     |              activations[0] contains the input samples.
     |          targets: The output targets of the output layer.
     |          layers: A list of Layers corresponding that generated the outputs in activations.
     |      Output:
     |          A list of parameter gradients where the gradients at each index corresponds to
     |          the parameters gradients of the layer at the same index in layers.
     |  
     |  forward_step(self, input_samples)
     |      Compute and return the forward activation of each layer in layers.
     |      Input:
     |          input_samples: A matrix of input samples (each row is an input vector)
     |          layers: A list of Layers
     |      Output:
     |          A list of activations where the activation at each index i+1 corresponds to
     |          the activation of layer i in layers. activations[0] contains the input samples.
     |  
     |  test(self, X_test, T_test)
     |      # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     |      # Get results of test data
     |      # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     |  
     |  train(self, X_train, T_train, X_validation, T_validation, learning_rate, max_iterations)
     |      # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     |      # train the network
     |      # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     |  
     |  update_params(self, param_grads, learning_rate)
     |      Function to update the parameters of the given layers with the given gradients
     |      by gradient descent with the given learning rate.
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)










NAME
    DataLoader

FILE
    e:\git\machinelearningex3\dataloader.py

FUNCTIONS
    formatClasses(classes, width)
    
    get_splitted_data(all_images, images_classes)
        #------------------------------------------------------
    
    loadImagesByList(file_pattern)
        #---------------------------------------------------
    
    load_cifar_images()
        #------------------------------------------------------
    
    load_cyst_images()
        #------------------------------------------------------
    
    load_mnist_images()
        loads hand-writing digits data from package sklearn
        :return: digits images and labels as: [ (8x8 pixels matrix, label), ... ]
    
    unpickle(file)
        #------------------------------------------------------

DATA
    CYST_CYST_FILE_PATTERN = '*Cyst*axial.png'
    CYST_OK_FILE_PATTERN = '*OK*axial.png'








NAME
    ex3

FILE
    e:\git\machinelearningex3\ex3.py

FUNCTIONS
    plotConfusionTable(y_true, y_pred, columns)
        Plots table of predicts for test data
        :param y_true: real clases
        :param y_pred: predicted classes
        :param columns: number of possible classes
    
    plotTrainCosts(iterations, batches, minibatch_costs, training_costs, validation_costs)
        Plots costs graph for training iterations
        :param iterations: number of iterations
        :param batches: number of batches
        :param minibatch_costs: array of costs for each batch in each iteration
        :param training_costs: array of training costs
        :param validation_costs: array of costs of validation data
