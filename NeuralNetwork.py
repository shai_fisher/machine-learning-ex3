import collections
import numpy as np
import itertools
from sklearn import metrics
from Layers import LinearLayer, LogisticLayer, SoftmaxOutputLayer
import time


class NeuralNetwork(object):

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Define the network
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, layers_input_sizes):
        self.layers = []
        i = 0
        #print layers_input_sizes
        while i < len(layers_input_sizes) - 2:
            #print layers_input_sizes[i], layers_input_sizes[i+1]
            self.layers.append(LinearLayer(layers_input_sizes[i], layers_input_sizes[i+1]))
            self.layers.append(LogisticLayer())
            i += 1
        #print layers_input_sizes[-2], layers_input_sizes[-1]
        self.layers.append(LinearLayer(layers_input_sizes[-2], layers_input_sizes[-1]))
        self.layers.append(SoftmaxOutputLayer())

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # forward
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Define the forward propagation step as a method.
    def forward_step(self, input_samples):
        """
        Compute and return the forward activation of each layer in layers.
        Input:
            input_samples: A matrix of input samples (each row is an input vector)
            layers: A list of Layers
        Output:
            A list of activations where the activation at each index i+1 corresponds to
            the activation of layer i in layers. activations[0] contains the input samples.
        """
        activations = [input_samples] # List of layer activations
        # Compute the forward activations for each layer starting from the first
        X = input_samples
        for layer in self.layers:
            Y = layer.get_output(X)  # Get the output of the current layer
            activations.append(Y)  # Store the output for future processing
            X = activations[-1]  # Set the current input as the activations of the previous layer
        return activations  # Return the activations of each layer

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Backward step
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Define the backward propagation step as a method
    def backward_step(self, activations, targets):
        """
        Perform the backpropagation step over all the layers and return the parameter gradients.
        Input:
            activations: A list of forward step activations where the activation at
                each index i+1 corresponds to the activation of layer i in layers.
                activations[0] contains the input samples.
            targets: The output targets of the output layer.
            layers: A list of Layers corresponding that generated the outputs in activations.
        Output:
            A list of parameter gradients where the gradients at each index corresponds to
            the parameters gradients of the layer at the same index in layers.
        """
        param_grads = collections.deque()  # List of parameter gradients for each layer
        output_grad = None  # The error gradient at the output of the current layer
        # Propagate the error backwards through all the layers.
        #  Use reversed to iterate backwards over the list of layers.
        for layer in reversed(self.layers):
            Y = activations.pop()  # Get the activations of the last layer on the stack
            # Compute the error at the output layer.
            # The output layer error is calculated different then hidden layer error.
            if output_grad is None:
                input_grad = layer.get_input_grad(Y, targets)
            else:  # output_grad is not None (layer is not output layer)
                input_grad = layer.get_input_grad(Y, output_grad)
            # Get the input of this layer (activations of the previous layer)
            X = activations[-1]
            # Compute the layer parameter gradients used to update the parameters
            grads = layer.get_params_grad(X, output_grad)
            param_grads.appendleft(grads)
            # Compute gradient at output of previous layer (input of current layer):
            output_grad = input_grad
        return list(param_grads)  # Return the parameter gradients

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # update_params
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Define a method to update the parameters
    def update_params(self, param_grads, learning_rate):
        """
        Function to update the parameters of the given layers with the given gradients
        by gradient descent with the given learning rate.
        """
        for layer, layer_backprop_grads in zip(self.layers, param_grads):
            for param, grad in itertools.izip(layer.get_params_iter(), layer_backprop_grads):
                # The parameter returned by the iterator point to the memory space of
                #  the original layer and can thus be modified inplace.
                param -= learning_rate * grad  # Update each parameter


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # train the network
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    def train(self, X_train, T_train, X_validation, T_validation, learning_rate, max_iterations):

        batch_size = 25     # Approximately 25 samples per batch - 25
        nb_of_batches = X_train.shape[0] / batch_size   # Number of batches
        xt_batches = zip(
            np.array_split(X_train, nb_of_batches, axis=0), # X samples
            np.array_split(T_train, nb_of_batches, axis=0)  # Y targets
        )

        # Perform backpropagation
        # initalize some lists to store the cost for future analysis
        minibatch_costs = []
        training_costs = []
        validation_costs = []

        # Train for the maximum number of iterations
        for iteration in range(max_iterations):
            for X, T in xt_batches:  # For each minibatch sub-iteration
                activations = self.forward_step(X)  # Get the activations
                minibatch_cost = self.layers[-1].get_cost(activations[-1], T)  # Get cost
                #print "minibatch_cost:", minibatch_cost
                minibatch_costs.append(minibatch_cost)
                param_grads = self.backward_step(activations, T)  # Get the gradients
                self.update_params(param_grads, learning_rate)  # Update the parameters
            print "itaration:", iteration, 'of', max_iterations, time.ctime(), ', ', minibatch_costs[-1]

            # Get full training cost for future analysis (plots)
            activations = self.forward_step(X_train)
            train_cost = self.layers[-1].get_cost(activations[-1], T_train)
            #print "train cost:", train_cost
            training_costs.append(train_cost)
            # Get full validation cost
            activations = self.forward_step(X_validation)
            validation_cost = self.layers[-1].get_cost(activations[-1], T_validation)
            validation_costs.append(validation_cost)
            if len(validation_costs) > 3:
                # Stop training if the cost on the validation set doesn't decrease
                #  for 3 iterations
                if validation_costs[-1] >= validation_costs[-2] >= validation_costs[-3]:
                    break

        nb_of_iterations = iteration + 1  # The number of iterations that have been executed
        return nb_of_iterations, nb_of_batches, minibatch_costs,training_costs, validation_costs

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Get results of test data
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    def test(self, X_test, T_test):
        y_true = np.argmax(T_test, axis=1)  # Get the target outputs
        activations = self.forward_step(X_test)  # Get activation of test samples
        y_pred = np.argmax(activations[-1], axis=1)  # Get the predictions made by the network
        test_accuracy = metrics.accuracy_score(y_true, y_pred)  # Test set accuracy
        print('The accuracy on the test set is {:.2f}'.format(test_accuracy))
        return y_true, y_pred